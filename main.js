/* jshint esnext: true */

require('./core/registry');

require('./core/DriverManager');

require('./core/RESTManager');

require('./drivers');
