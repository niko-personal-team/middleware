/* jshint esnext: true */
let
  express = require('express'),
  app = express(),
  registry = require('./registry'),
  cors = require('cors'),
  server
  ;

  app.use(cors())

let RESTManager = {
  init() {

    this
      .defineRoutes()
      .startServer()
      ;

    return this;
  },
  defineRoutes() {

    // definisco le routes per il middleware

    // route statica, per i contenuti dell'interfaccia
    app.use(express.static('public'));

    // app.get('/', function(req, res){
    // 	res.send('IoT Middleware - 0.1');
    // });


    // mostro un device oppure tutti, se non viene fornito l'id
    app.route('/devices/:id')
      .get(function (req, res) {
        let
          id = req.params.id,
          devices = registry.getDeviceById(id)
          ;

        res.json({ 'devices': devices });
      })
      ;
    app.route('/devices/:id/:property/:val')
      // todo: questa dovrebbe andare in post
      .get(function (req, res) {
        let id = req.params.id,
          property = req.params.property,
          val = req.params.val
          ;
        console.log(registry.getDeviceById(id));
        let computedVal;
        switch (true) {
          case val === 'true':
            computedVal = true;
            break;
          case val === 'false':
            computedVal = false;
            break;
          case !isNaN(val):
            computedVal = +val;
            break;
          default:
            computedVal = val;
            break;
        }
        registry.getDeviceById(id)[property] = computedVal;
        res.json({});
      })
      ;
    app.route('/devices')
      .get(function (req, res) {
        let devices = registry.getDevices();

        res.json({ 'devices': devices });
      })
      ;

    app.route('/devices/bytype/:type')
      .get((req, res) => {
        let
          type = req.params.type,
          devices = registry.getDevicesByType(type)
          ;
        res.json({ 'devices': devices });
      });


    return this;
  },
  startServer() {
    server = app.listen(3000, function () {
      let address = server.address();
      console.log('Middleware listening at http://%s:%s', address.address, address.port);
    });
  }
};


// facciamo partire il web server


module.exports = RESTManager.init();