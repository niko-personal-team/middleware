/* jshint esnext:true */

let pubsub = require('pubsub-js');

let Lamp = require('../Things/Lamp');
let ColorLamp = require('../Things/ColorLamp');

let 
	declaredClasses = {},
	devicesList = [],
	devicesMap   = {}, 
	registry     = {
		onNewDevice(topic, device){
			// console.log('[ registry ] new device added', device);
			// a questo punto il dispositivo fa parte della lista ed è ricercabile
			devicesList.push(device);
		},
		onClassDeclared(topic, args){
			let ctorName = args.constructor.name,
				ctor = args.constructor
			;

      console.log('2) declaring...', ctor.name);
			declaredClasses[ctorName] = ctor;

			while(ctor !== Object && Object.getPrototypeOf(ctor) !== Object){
				ctor = Object.getPrototypeOf(ctor);
				ctorName = ctor.name;

				declaredClasses[ctorName] = ctor;
			}

			// console.log('declaredClasses = ', declaredClasses);
		},
		subscribeTopics(){
			pubsub.subscribe('/devices/new', this.onNewDevice.bind(this));
			pubsub.subscribe('/system/declaredclass', this.onClassDeclared.bind(this));
		},
		init(){
			this.subscribeTopics();
			return this;
		},
		getDevices(){
			return devicesList.slice(0);
		},

		getDeviceById: id =>  devicesList.slice(0).filter(device => device.id === +id)[0],

		getDevicesByType: type => 
			declaredClasses[type] ? devicesList.slice(0).filter(device => device instanceof declaredClasses[type]) : []

};

module.exports = registry.init();