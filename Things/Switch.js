/* jshint esnext: true */

let Thing = require('./Thing');

let Switch = class Switch extends Thing{
	constructor(args){
		super(args);
	}
	set on(/** Boolean */ status){
		this.driver.on = { phid: this.phid, status };
	}
	get on(){
		return this.driver.getOn(this.phid);
	}
};

module.exports = Switch;