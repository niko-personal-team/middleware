/* jshint esnext: true */

let Lamp = require('./Lamp');

let RGBLamp = class RGBLamp extends Lamp{
	constructor(args){
		super();
	}
	set rgb(args){
		this.driver.rgb = args;
	}
	get rgb(){

	}
};
module.exports = RGBLamp;