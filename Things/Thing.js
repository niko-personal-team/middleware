/* jshint esnext:true */
const pubsub = require('pubsub-js');

let counter = 0;
class Thing extends Object {
	constructor(args){
		super();
		this.phid = args.phid;
		this.id   = counter++;
		this.type = this.constructor.name;

		// uso defineProperty perché risulta fastidioso vedere l'intero driver nei singoli oggetti nel json risultante
		// da una chiamata GET
		Object.defineProperty(this, 'driver', {
			value: args.driver,
			configurable: false, // messo per chiarezza, false è il default 
			enumerable: false    // come sopra, default è false
		});

		pubsub.publish('/system/declaredclass', {
			name: this.constructor.name,
			constructor: this.constructor
		});
	}
}

module.exports = Thing;