/* jshint esnext: true */

let Lamp = require('./Lamp');

let ColorLamp = class ColorLamp extends Lamp {
	constructor(args){
		super(args);
	}
	set color(color){
    console.log('setting color to', color);
    this.driver.color = { phid: this.phid,  hue: color };
  }
};

module.exports = ColorLamp;