/* jshint esnext: true */

let Switch = require('./Switch');

let Lamp = class Lamp extends Switch {
	constructor(args){
		super(args);
	}
	set on(/** Boolean */ status){
		super.on = status;
	}
	get on(){
		return 0;
	}
};

module.exports = Lamp;