/* jshint esnext: true */

/* 
	non potrebbe essere noto qui
*/

let ColorLamp = require('../../Things/ColorLamp');
let pubsub = require('pubsub-js');
let http = require('http');

let
	instances = [], 
	// IP        = '192.168.2.115',
	IP        = '192.168.1.136',
	user      = '-OP3On9G-Lk3-yuI62JyEQSdPqrRfgTAm5lQ9t6O'
	// IP        = '192.168.0.10',
	// user      = '1fe032cafc0f51cd2d3923585197a7f0'
;

// fra le nostre lampade
// 2: faro

let Philips = class Philips {
	constructor(){
		var lamp;
		// TODO: automatizzare
		// 
		this.initializeLamps();

		return this;
	}
	initializeLamps(){
		let req, 
		options = {
			hostname: IP,
			path: '/api/' + user + '/lights',
			method: 'GET'
		};
		// /api/1234567890/lights
		return new Promise(function(resolve, reject){
			req = http.request(options, function(res){
				let result = '';
				res.on('data', function(partial){
					// console.log('partial is', partial.toString());
					result += partial;
				});
				res.on('end', function(){
          let lamps = JSON.parse(result);
					resolve(lamps);
				});
			});
			req.on('response', function(response){
			});
			req.on('error', function(error){
			});
			req.end();
		}).then(function(lamps){
			for(let i in lamps){
				let lamp;
				if(lamps.hasOwnProperty(i)){
					pubsub.publish('/devices/new', lamp = new ColorLamp({ phid: i, driver: this }));
					// questa potremmo riscriverla in modo che venga ereditata. Non ci sarebbe nemmeno bisogno di AOP
					// a questo punto
					instances.push(lamp);
				}

			}
		}.bind(this)).catch(function(err){
			console.error('error contating the HUE bridge', err);
			throw(err);
		});
  }
  set color(args){
    console.log('setting color hue to', args)
		let cmd = {}, 
		opts = {
			hostname: IP,
			path: '/api/' + user + '/lights/' + args.phid + '/state', // phid
			method: 'PUT'
		},
		p = new Promise(function(resolve, reject){
			let resData = '', 
			req = http.request(opts, function(res){
				res.on('data', function(partialRes){
					resData += partialRes;
				});
				res.on('end', function(){
					resolve(resData);
				});
			});
			// console.log(cmd)
			// cmd.on = (args.on === 'true' ? true : false);
      cmd.on = args.status;
      cmd.hue = args.hue;
      cmd.on = true;
			req.write(JSON.stringify(cmd));
			req.on('response', function(response){
			});
			req.on('error', function(error){
				console.log('ERROR:', error);
			});
			req.end();
		});
		return p;
  }
	set on(args){
		let cmd = {}, 
		opts = {
			hostname: IP,
			path: '/api/' + user + '/lights/' + args.phid + '/state', // phid
			method: 'PUT'
		},
		p = new Promise(function(resolve, reject){
			let resData = '', 
			req = http.request(opts, function(res){
				res.on('data', function(partialRes){
					resData += partialRes;
				});
				res.on('end', function(){
					resolve(resData);
				});
			});
			// console.log(cmd)
			// cmd.on = (args.on === 'true' ? true : false);
			cmd.on = args.status;
			//cmd.on = true;
			req.write(JSON.stringify(cmd));
			req.on('response', function(response){
			});
			req.on('error', function(error){
				console.log('ERROR:', error);
			});
			req.end();
		});
		return p;
		// TODO call the bridge
	}
	get on(){

	}
};

module.exports = new Philips();